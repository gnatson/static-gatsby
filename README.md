<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
<p align="center">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="60" />
    <img alt="Gatsby" src="screens/preview-1.png" width="300" />
</p>
<h1 align="center">
  Static Gatsby Template
</h1>

[![Netlify Status](https://api.netlify.com/api/v1/badges/c3ac5dea-8264-460f-ae85-4ce9451a5f9c/deploy-status)](https://app.netlify.com/sites/gnatson-static-gatsby/deploys)